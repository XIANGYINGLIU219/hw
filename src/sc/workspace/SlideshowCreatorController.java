package sc.workspace;

import djf.ui.AppMessageDialogSingleton;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;
import sc.SlideshowCreatorApp;
import static sc.SlideshowCreatorProp.APP_PATH_WORK;
import static sc.SlideshowCreatorProp.INVALID_IMAGE_PATH_MESSAGE;
import static sc.SlideshowCreatorProp.INVALID_IMAGE_PATH_TITLE;
import sc.data.Slide;
import sc.data.SlideshowCreatorData;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file
 * toolbar.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public class SlideshowCreatorController {
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    SlideshowCreatorApp app;
    jTPS  jtps;
    boolean isPlay;
    int currentIndex ;
     List<Slide> slideArray;
     int numTasks=0;
     ReentrantLock lock;
     int count=0;
    /**
     * Constructor, note that the app must already be constructed.
     */
    public SlideshowCreatorController(SlideshowCreatorApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        jtps=new jTPS();
        slideArray = new ArrayList<Slide>();
       lock=new ReentrantLock();
       currentIndex=0;
       isPlay=false;
    }
    
    public void handleSlideshowTitle(String init, String now){
      SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
    jTPS_Transaction  tansactions=new Change_Title(workspace,init,now);
         jtps.addTransaction(tansactions);
        app.getGUI().updateToolbarControls(false);
     
    }
    
    // CONTROLLER METHOD THAT HANDLES ADDING A DIRECTORY OF IMAGES
    public void handleAddAllImagesInDirectory() {  
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent(); 
        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
          List<Slide> list = new ArrayList<Slide>();
      
        try {  
            // ASK THE USER TO SELECT A DIRECTORY
            DirectoryChooser dirChooser = new DirectoryChooser();
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            dirChooser.setInitialDirectory(new File(props.getProperty(APP_PATH_WORK)));
            File dir = dirChooser.showDialog(app.getGUI().getWindow());
            
            if (dir != null) { 
                File[] files = dir.listFiles();
                for (File f : files) {
                    String fileName = f.getName();
                    if (fileName.toLowerCase().endsWith(".png") ||
                            fileName.toLowerCase().endsWith(".jpg") ||
                            fileName.toLowerCase().endsWith(".gif")) {
                        String path = f.getPath();
                        String caption = "";
                        Image slideShowImage = loadImage(path);
                        int originalWidth = (int)slideShowImage.getWidth();
                        int originalHeight = (int)slideShowImage.getHeight();
                        Slide sl=new Slide(fileName, path, caption, originalWidth, originalHeight);
                      if(!data.hasSlide(sl.getFileName(), sl.getPath())){
                          list.add(sl);
                      }
                        data.addNonDuplicateSlide(fileName, path, caption, originalWidth, originalHeight);
                        if(app.getGUI().getFileController().getCheck()){
                                        app.getGUI().updateToolbarControls(false);
                         }
                          app.getGUI().updateSavedAs(false);
                          workspace.undoButton.setDisable(false);
                          workspace.redoButton.setDisable(false);
                        
                    }
                }  
                ObservableList<Slide> obser= FXCollections.observableList(list);               
                jTPS_Transaction  tansactions=new Add_All_Image(data,workspace,obser);
                         jtps.addTransaction(tansactions);
                        
            }
        }
        catch(MalformedURLException murle) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String title = props.getProperty(INVALID_IMAGE_PATH_TITLE);
            String message = props.getProperty(INVALID_IMAGE_PATH_MESSAGE);
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(title, message);
        }
    }
    
    public Slide handleAddImage() {
         Slide target=null;
        try {
            // ASK THE USER TO SELECT A DIRECTORY
            FileChooser imageChooser = new FileChooser();
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            imageChooser.setInitialDirectory(new File(props.getProperty(APP_PATH_WORK)));
            File imageFile = imageChooser.showOpenDialog(app.getGUI().getWindow());
            if (imageFile != null) {
                String fileName = imageFile.getName();
                if (fileName.toLowerCase().endsWith(".png") ||
                            fileName.toLowerCase().endsWith(".jpg") ||
                            fileName.toLowerCase().endsWith(".gif")) {
                   
                    String path = imageFile.getPath();
                    String caption = "";
                    Image slideShowImage = loadImage(path);
                    int originalWidth = (int)slideShowImage.getWidth();
                    int originalHeight = (int)slideShowImage.getHeight();
                    SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
                     SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
                      ObservableList<Slide> tableData = data.getSlides();
                      boolean check=true;
                      for(Slide si: tableData){
                              if(si.getFileName().equals(fileName)){
                                  check=false;
                              }
                        }
                    data.addNonDuplicateSlide(fileName, path, caption, originalWidth, originalHeight);
                     if(check){
                         for(int i=0; i<tableData.size();i++){
                              if(tableData.get(i).getFileName().equals(fileName)){
                                  target=tableData.get(i);
                              }
                        }
                         if(app.getGUI().getFileController().getCheck()){
                      app.getGUI().updateToolbarControls(false);
                         }
                        
                      app.getGUI().updateSavedAs(false);
                     jTPS_Transaction  tansactions=new Add_Image(data, workspace,fileName, path, caption, originalWidth, originalHeight);
                     jtps.addTransaction(tansactions);
                }
                }
            }
            
        }
        catch(MalformedURLException murle) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String title = props.getProperty(INVALID_IMAGE_PATH_TITLE);
            String message = props.getProperty(INVALID_IMAGE_PATH_MESSAGE);
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(title, message);
        }
        return target;
    }
    
    public void handleRemoveImage() { 
        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
        TableView slidesTableView = workspace.slidesTableView;
        Slide selectedSlide = (Slide)slidesTableView.getSelectionModel().getSelectedItem();
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        int index=slidesTableView.getSelectionModel().getSelectedIndex();
        data.remove(selectedSlide.getFileName(),selectedSlide.getPath());
        if(app.getGUI().getFileController().getCheck()){
                      app.getGUI().updateToolbarControls(false);
                         }
       app.getGUI().updateSavedAs(false);
        jTPS_Transaction  tansactions=new Remove_Image(data, workspace,selectedSlide, index);
        jtps.addTransaction(tansactions); 
    }
    
    public void handleUpdateSlide() {
        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
        workspace.updateButton.setDisable(true); 
        TableView slidesTableView = workspace.slidesTableView;
        Slide selectedSlide = (Slide)slidesTableView.getSelectionModel().getSelectedItem();
        int initCurrentWidth=selectedSlide.getCurrentWidth();
         int initCurrentHeight=selectedSlide.getCurrentHeight();
        int initXPosition=selectedSlide.getXPosition();
        int initYPosition=selectedSlide.getYPosition();
        String initCaption=selectedSlide.getCaption();
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        
        String caption = workspace.captionTextField.getText();
        int currentWidth = (int)workspace.currentWidthSlider.getValue();
        int currentHeight = (int)workspace.currentHeightSlider.getValue();
        int xPosition= (int)workspace.xPositionSlider.getValue();
        int yPosition= (int)workspace.yPositionSlider.getValue();
        selectedSlide.setCaption(caption);
        selectedSlide.setCurrentWidth(currentWidth);
        selectedSlide.setCurrentHeight(currentHeight);
        selectedSlide.setXPosition(xPosition);
        selectedSlide.setYPosition(yPosition);
         int newCurrentWidth=selectedSlide.getCurrentWidth();
         int newCurrentHeight=selectedSlide.getCurrentHeight();
        int newXPosition=selectedSlide.getXPosition();
        int newYPosition=selectedSlide.getYPosition();
        String newCaption=selectedSlide.getCaption();
       if(app.getGUI().getFileController().getCheck()){
                      app.getGUI().updateToolbarControls(false);
                         }
        app.getGUI().updateSavedAs(false);
        jTPS_Transaction  tansactions=new Update_Image(workspace,selectedSlide,initCaption, initCurrentWidth, initCurrentHeight,initXPosition,initYPosition,newCaption, newCurrentWidth, newCurrentHeight,newXPosition,newYPosition);
         jtps.addTransaction(tansactions);       
    }
    
    public void handleSelectSlide() throws MalformedURLException {
        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
        Slide selectedSlide = workspace.slidesTableView.getSelectionModel().getSelectedItem();
        int selectedIndex = workspace.slidesTableView.getSelectionModel().getSelectedIndex();
        
        // WE ONLY RESPOND IF IT'S A SELECTION
        if (selectedIndex >= 0) {

            // LOAD ALL THE SLIDE DATA INTO THE CONTROLS
            workspace.fileNameTextField.setText(selectedSlide.getFileName());
            workspace.pathTextField.setText(selectedSlide.getPath());
            workspace.captionTextField.setText(selectedSlide.getCaption());
            workspace.originalWidthTextField.setText("" + selectedSlide.getOriginalWidth());
            workspace.originalHeightTextField.setText("" + selectedSlide.getOriginalHeight());
            workspace.currentWidthSlider.setValue(selectedSlide.getCurrentWidth().doubleValue());
            workspace.currentHeightSlider.setValue(selectedSlide.getCurrentHeight().doubleValue());
            workspace.xPositionSlider.setValue(selectedSlide.getXPosition().doubleValue());
            workspace.yPositionSlider.setValue(selectedSlide.getYPosition().doubleValue());
            Image slideImage=loadImage(selectedSlide.getPath());
            ImageView view=new ImageView(slideImage);
           workspace.imagePane.setRight(view);
        }
    }
    


    void handleCaptionTextTyped() {
        SlideshowCreatorWorkspace gui = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
        gui.updateButton.setDisable(false);
    }
    
    void handleSliderMoved() {
        SlideshowCreatorWorkspace gui = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
        gui.updateButton.setDisable(false);        
    }
    
    public void handleUp(){
         SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
        int index=workspace.getSlideTable().getSelectionModel().getSelectedIndex();
        Slide sl=(Slide)workspace.getSlideTable().getSelectionModel().getSelectedItem();
         ObservableList<Slide> tableData = data.getSlides();
         tableData.remove(sl);
         tableData.add(index-1, sl);
         workspace.getSlideTable().requestFocus();
         workspace.getSlideTable().getSelectionModel().select(sl);
          jTPS_Transaction  tansactions=new Move_Up(data,workspace,index, sl);
          jtps.addTransaction(tansactions);
        // app.getGUI().updateSavedAs(false);
        //app.getGUI().updateToolbarControls(false);
        
    }
    
    public void handleDown(){
         SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
         ObservableList<Slide> tableData = data.getSlides();
         int index=workspace.getSlideTable().getSelectionModel().getSelectedIndex();
        Slide sl=(Slide)workspace.getSlideTable().getSelectionModel().getSelectedItem();
         tableData.remove(sl);
         tableData.add(index+1, sl);
         workspace.getSlideTable().requestFocus();
         workspace.getSlideTable().getSelectionModel().select(sl);
        jTPS_Transaction  tansactions=new Move_Down(data,workspace,index, sl);
          jtps.addTransaction(tansactions);
          // app.getGUI().updateSavedAs(false);
        //app.getGUI().updateToolbarControls(false);
        
    }
    public void handleDoTransaction(){
          count--;
         jtps.doTransaction();  
         if(count==0){SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
        workspace.redoButton.setDisable(true);
}
    }
    
    public void handleUnDoTransaction(){
       count++;
        jtps.undoTransaction();
    }
    
    public void handleViewSlideshow() throws MalformedURLException {
        isPlay=false;
        slideArray.clear();
        currentIndex=0;
        SlideshowCreatorWorkspace workspace=(SlideshowCreatorWorkspace)app.getWorkspaceComponent();
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        //Get Buttons
       Button playButton=workspace.playButton;
       Button stopButton=workspace.stopButton;
       Button nextButton=workspace.nextButton;
       Button previousButton=workspace.previousButton;
         Label slideCaption= new Label();
         slideCaption.setStyle(" -fx-font-size:18pt;  -fx-padding: 10 15 10 15;  -fx-spacing: 10;");
         ImageView slideImageView = new ImageView();
        ObservableList<Slide> tableData = workspace.getSlideTable().getItems();
       for(int i=0; i< tableData.size(); i++){
           slideArray.add(tableData.get(i));
       }
	// CUSTOMIZE OUR IMAGE VIEW
	Image startImage = loadImage(slideArray.get(currentIndex).getPath()); 
        int xPosition=slideArray.get(currentIndex).getXPosition();
        int yPosition=slideArray.get(currentIndex).getYPosition();
        int width=slideArray.get(currentIndex).getCurrentWidth();
         int height=slideArray.get(currentIndex).getCurrentHeight();
        if(slideArray.get(currentIndex).getCaption().equals("")){
            slideCaption.setText("None");
        }
        else{
	slideCaption.setText(slideArray.get(currentIndex).getCaption());
        }
	slideImageView.setImage(startImage);
	// PUT THEM IN A CONTAINER
        workspace.viewBox.setTranslateX(xPosition);
        workspace.viewBox.setTranslateY(yPosition);
         slideImageView.setFitWidth(width);
         slideImageView.setFitHeight(height);
	 workspace.setVBox(slideImageView);
          workspace.setVBox(slideCaption);
	// PROVIDE A RESPONSE TO BUTTON CLICK
        playButton.setOnAction(e->{
            handlePlay(true);
        });
        
        stopButton.setOnAction(e->{
            handlePlay(false);
            
        });
        
        nextButton.setOnAction(e->{
            try {
                if(currentIndex==tableData.size()-1){
                    currentIndex=-1;
                }
                currentIndex++;
                handlePlay(isPlay);
                Slide initSl=tableData.get(currentIndex);
                slideCaption.setText(initSl.getCaption());
                Image initImage = loadImage(initSl.getPath());
                slideImageView.setImage(initImage);
                workspace.viewBox.getChildren().clear();
                slideImageView.setTranslateX(initSl.getXPosition());
                slideImageView.setTranslateY(initSl.getYPosition());
                 slideImageView.setFitWidth(initSl.getCurrentWidth());
                 slideImageView.setFitWidth(initSl.getCurrentHeight());
                workspace.setVBox(slideImageView);
                workspace.setVBox(slideCaption);
            } catch (MalformedURLException ex) {
                Logger.getLogger(SlideshowCreatorController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        previousButton.setOnAction(e->{
            try {
                if(currentIndex==0){
                    currentIndex=tableData.size();
                }
                currentIndex--;
                Slide initSl=tableData.get(currentIndex);
                slideCaption.setText(initSl.getCaption());
                Image initImage = loadImage(initSl.getPath());
                slideImageView.setImage(initImage);
                workspace.viewBox.getChildren().clear();
                slideImageView.setTranslateX(initSl.getXPosition());
                slideImageView.setTranslateY(initSl.getYPosition());
                slideCaption.setTranslateX(initSl.getXPosition());
                slideCaption.setTranslateY(initSl.getYPosition());
                  slideImageView.setFitWidth(initSl.getCurrentWidth());
                slideImageView.setFitWidth(initSl.getCurrentHeight());
                  workspace.setVBox(slideImageView);
                workspace.setVBox(slideCaption);
                handlePlay(isPlay);
            } catch (MalformedURLException ex) {
                Logger.getLogger(SlideshowCreatorController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
                }
           private void handlePlay(boolean play){
               SlideshowCreatorWorkspace workspace=(SlideshowCreatorWorkspace)app.getWorkspaceComponent(); 
               ObservableList<Slide> tableData = workspace.getSlideTable().getItems();
             Label slideCaption= new Label();
            ImageView slideImageView = new ImageView();
            isPlay=play;
            if(isPlay){
             Task<Void> task = new Task<Void>() {
                 
                    int task = numTasks++;
                    @Override
                    protected Void call() throws Exception {
                        try {
                              lock.lock(); 
                            if(currentIndex==tableData.size()-1){
                                currentIndex=-1;
                            }
                            while(isPlay) {
                            // THIS WILL BE DONE ASYNCHRONOUSLY VIA MULTITHREADING
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    try { 
                                        currentIndex++;
                                        if(currentIndex==tableData.size()){
                                            currentIndex=0;
                                        }
                                        Slide initSl=tableData.get(currentIndex);
                                        slideCaption.setText(initSl.getCaption());
                                        Image initImage = loadImage(initSl.getPath());
                                        slideImageView.setImage(initImage);
                                         slideImageView.setTranslateX(initSl.getXPosition());
                                          slideImageView.setTranslateY(initSl.getYPosition());
                                            slideImageView.setFitWidth(initSl.getCurrentWidth());
                                             slideImageView.setFitWidth(initSl.getCurrentHeight());
                                        workspace.viewBox.getChildren().clear();
                                          workspace.setVBox(slideImageView);
                                          workspace.setVBox(slideCaption);
                                       
                                    } catch (MalformedURLException ex) {
                                        Logger.getLogger(SlideshowCreatorController.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                            });
                            
                            // SLEEP EACH FRAME
                            Thread.sleep(2000);
                        }
                        }
                        finally {
			   lock.unlock();
                                }
                        return null;
                       
                    }
                   
                };
                // THIS GETS THE THREAD ROLLING  
           
                Thread thread = new Thread(task);
                thread.start();   
            } 
           }      
   
// THIS HELPER METHOD LOADS AN IMAGE SO WE CAN SEE IT'S SIZE
    private Image loadImage(String imagePath) throws MalformedURLException {
	File file = new File(imagePath);
	URL fileURL = file.toURI().toURL();
	Image image = new Image(fileURL.toExternalForm());
	return image;
    }
}