/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sc.workspace;

import djf.ui.AppMessageDialogSingleton;
import jtps.jTPS_Transaction;
import sc.data.Slide;

/**
 *
 * @author xiangying liu
 */
public class Update_Image implements jTPS_Transaction{
    private SlideshowCreatorWorkspace workspace;
    private Slide sl;
    private String caption;
    private String newCaption;
    private int currentWidth;
    private int currentHeight;
     private int xPosition;
      private int yPosition;
       private int newCurrentWidth;
    private int newCurrentHeight;
     private int newXPosition;
      private int newYPosition;
        
    public Update_Image(SlideshowCreatorWorkspace initWorkspace, Slide initSl, String initCaption , int iCurrentWidth, int iCurrentHeight,int iXPosition,int iYPosition,
            String nnewCaption, int nnewCurrentWidth,int nnewCurrentHeight,int nnewXPosition,int nnewYPosition){
        workspace=initWorkspace;
        sl=initSl;
        caption=initCaption;
        currentWidth=iCurrentWidth;
        currentHeight=iCurrentHeight;
        xPosition=iXPosition;
        yPosition=iYPosition;
         newCaption=nnewCaption;
        newCurrentWidth=nnewCurrentWidth;
        newCurrentHeight=nnewCurrentHeight;
        newXPosition=nnewXPosition;
        newYPosition=nnewYPosition;
    }

    @Override
    public void doTransaction() {
       workspace.getSlideTable().getSelectionModel().clearSelection();
        sl.setCaption(newCaption);
        sl.setCurrentWidth(newCurrentWidth);
        sl.setCurrentHeight(newCurrentHeight);
        sl.setXPosition(newXPosition);
        sl.setYPosition(newYPosition);
         workspace.slidesTableView.refresh();
         workspace.getSlideTable().requestFocus();
         workspace.getSlideTable().getSelectionModel().select(sl);

    }

    @Override
    public void undoTransaction() {
          workspace.getSlideTable().getSelectionModel().clearSelection();
        sl.setCaption(caption);
        sl.setCurrentWidth(currentWidth);
        sl.setCurrentHeight(currentHeight);
        sl.setXPosition(xPosition);
        sl.setYPosition(yPosition);
        workspace.getSlideTable().refresh();
        workspace.getSlideTable().requestFocus();
        workspace.getSlideTable().getSelectionModel().select(sl);
     
    }
    
}
