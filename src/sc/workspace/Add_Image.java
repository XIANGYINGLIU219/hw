/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sc.workspace;

import djf.ui.AppMessageDialogSingleton;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import sc.data.Slide;
import sc.data.SlideshowCreatorData;

/**
 *
 * @author xiangying liu
 */
public class Add_Image implements jTPS_Transaction {
    SlideshowCreatorData data;
    SlideshowCreatorWorkspace workspace;
     String fileName;
     String path;
     String caption;
     int originalWidth;
     int originalHeight;

   public Add_Image(SlideshowCreatorData initData, SlideshowCreatorWorkspace initWorkspace,String initFileName, String initPath, String initCaption,int  initOriginalWidth, int initOriginalHeight) {
       data=initData;
       workspace=initWorkspace;
       fileName=initFileName;
       path=initPath;
       caption=initCaption;
       originalWidth=initOriginalWidth;
       originalHeight=initOriginalHeight;
    }


    @Override
    public void doTransaction() {
        ObservableList<Slide> tableData = data.getSlides();
       data.addNonDuplicateSlide(fileName, path, caption, originalWidth, originalHeight);
        for(Slide si: tableData){
                              if(si.getFileName().equals(fileName))
                                  workspace.getSlideTable().getSelectionModel().select(si);
                        }
       workspace.getSlideTable().requestFocus();
      
       
    }
    @Override
    public void undoTransaction() {
       data.remove(fileName, path);
       workspace.getSlideTable().refresh();
      workspace.clearUpdateField();
      workspace.getSlideTable().getSelectionModel().clearSelection();
    }
}
