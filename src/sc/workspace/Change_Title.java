/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sc.workspace;

import djf.ui.AppMessageDialogSingleton;
import jtps.jTPS_Transaction;

/**
 *
 * @author xiangying liu
 */
public class Change_Title implements jTPS_Transaction {
    private SlideshowCreatorWorkspace workspace;
    private String old;
    private String now;
    int i=0;
    
    public Change_Title( SlideshowCreatorWorkspace initWorkspace, String initOld, String initNow){
        workspace=initWorkspace;
        old=initOld;
        now=initNow;
    }

    @Override
    public void doTransaction() {
        workspace.getSlideshowTitle().setText(now);
    }

    @Override
    public void undoTransaction() {
        workspace.getSlideshowTitle().setText(old);
    }
    
}
