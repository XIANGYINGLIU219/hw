    package sc.workspace;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static djf.settings.AppPropertyType.NEXT_TOOLTIP;
import static djf.settings.AppPropertyType.PLAY_TOOLTIP;
import static djf.settings.AppPropertyType.PREVIOUS_TOOLTIP;
import static djf.settings.AppPropertyType.STOP_TOOLTIP;
import static djf.ui.AppGUI.CLASS_BORDERED_PANE;
import djf.ui.AppMessageDialogSingleton;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.EventType;
import javafx.geometry.Pos;
import javafx.scene.Node;
import sc.SlideshowCreatorApp;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import static sc.SlideshowCreatorProp.ADD_ALL_IMAGES_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.ADD_IMAGE_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.CAPTION_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_HEIGHT_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_HEIGHT_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_WIDTH_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_WIDTH_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.FILE_NAME_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.FILE_NAME_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.MOVE_DOWN_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.MOVE_UP_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.ORIGINAL_HEIGHT_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.ORIGINAL_WIDTH_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.PATH_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.REDO_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.REMOVE_IMAGE_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.SLIDES_TITLE_TEXT;
import static sc.SlideshowCreatorProp.UNDO_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.UPDATE_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.X_POSITION_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.X_POSITION_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.Y_POSITION_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.Y_POSITION_PROMPT_TEXT;
import sc.data.Slide;
import sc.data.SlideshowCreatorData;
import static sc.style.SlideshowCreatorStyle.CLASS_EDIT_BUTTON;
import static sc.style.SlideshowCreatorStyle.CLASS_EDIT_SLIDER;
import static sc.style.SlideshowCreatorStyle.CLASS_EDIT_TEXT_FIELD;
import static sc.style.SlideshowCreatorStyle.CLASS_PROMPT_LABEL;
import static sc.style.SlideshowCreatorStyle.CLASS_SLIDES_TABLE;
import static sc.style.SlideshowCreatorStyle.CLASS_UPDATE_BUTTON;

/**
 * This class serves as the workspace component for the TA Manager
 * application. It provides all the user interface controls in 
 * the workspace area.
 * 
 * @author Richard McKenna
 */
public class SlideshowCreatorWorkspace extends AppWorkspaceComponent {
    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    SlideshowCreatorApp app;
int count=0;
   VBox viewBox;
    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    SlideshowCreatorController controller;
    Button stopButton;
    Button nextButton;
     Button previousButton;
     Button playButton;
    Label title;
    BorderPane imagePane;
    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    HBox editImagesToolbar;
    Button addAllImagesInDirectoryButton;
    Button addImageButton;
    Button removeImageButton;
    TextField slideshowTitle;
    Button moveUpButton;
    Button moveDownButton;
    Button undoButton;
    Button redoButton;
    
    // FOR THE SLIDES TABLE
    ScrollPane slidesTableScrollPane;
    TableView<Slide> slidesTableView;
    TableColumn<Slide, StringProperty> fileNameColumn;
    TableColumn<Slide, IntegerProperty> currentWidthColumn;
    TableColumn<Slide, IntegerProperty> currentHeightColumn;
    TableColumn<Slide, IntegerProperty> xPositionColumn;
    TableColumn<Slide, IntegerProperty> yPositionColumn;

    // THE EDIT PANE
    ScrollPane scrollEditPane;
    GridPane editPane;
    Label fileNamePromptLabel;
    TextField fileNameTextField;
    Label pathPromptLabel;
    TextField pathTextField;
    Label captionPromptLabel;
    TextField captionTextField;
    Label originalWidthPromptLabel;
    TextField originalWidthTextField;
    Label originalHeightPromptLabel;
    TextField originalHeightTextField;
    Label currentWidthPromptLabel;
    Slider currentWidthSlider;
    Label currentHeightPromptLabel;
    Slider currentHeightSlider;
     Label xPositionPromptLabel;
    Slider xPositionSlider;
    Label  yPositionPromptLabel;
    Slider yPositionSlider;
    Button updateButton;
    Button viewButton;
    String origin;
    
    /**
     * The constructor initializes the user interface for the
     * workspace area of the application.
     */
    public SlideshowCreatorWorkspace(SlideshowCreatorApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        viewBox=new VBox();
        origin=new String("");
        viewButton=app.getGUI().getViewButton();
        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        playButton=new Button(props.getProperty(PLAY_TOOLTIP));
        stopButton=new Button(props.getProperty(STOP_TOOLTIP));
         nextButton=new Button(props.getProperty(NEXT_TOOLTIP));
          previousButton=new Button(props.getProperty(PREVIOUS_TOOLTIP));
        // LAYOUT THE APP
        initLayout();
        
        // HOOK UP THE CONTROLLERS
        initControllers();
        
        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();
    }
    
    private void initLayout() {
        // WE'LL USE THIS TO GET UI TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        imagePane=new BorderPane();
        // FIRST MAKE ALL THE COMPONENTS
        editImagesToolbar = new HBox();
        addAllImagesInDirectoryButton = new Button(props.getProperty(ADD_ALL_IMAGES_BUTTON_TEXT));
        addImageButton = new Button(props.getProperty(ADD_IMAGE_BUTTON_TEXT));
        removeImageButton = new Button(props.getProperty(REMOVE_IMAGE_BUTTON_TEXT));
        moveUpButton = new Button(props.getProperty(MOVE_UP_BUTTON_TEXT));
        moveDownButton = new Button(props.getProperty(MOVE_DOWN_BUTTON_TEXT));
        undoButton= new Button(props.getProperty(UNDO_BUTTON_TEXT));
        redoButton= new Button(props.getProperty(REDO_BUTTON_TEXT));
        slideshowTitle=new TextField();
        slideshowTitle.setPromptText(props.getProperty(SLIDES_TITLE_TEXT));
        title=new Label();
        slidesTableScrollPane = new ScrollPane();
        scrollEditPane = new ScrollPane();
        slidesTableView = new TableView();
        editPane = new GridPane();
        fileNamePromptLabel = new Label(props.getProperty(FILE_NAME_PROMPT_TEXT));
        fileNameTextField = new TextField();
        pathPromptLabel = new Label(props.getProperty(PATH_PROMPT_TEXT));
        pathTextField = new TextField();
        captionPromptLabel = new Label(props.getProperty(CAPTION_PROMPT_TEXT));
        captionTextField = new TextField();
        originalWidthPromptLabel = new Label(props.getProperty(ORIGINAL_WIDTH_PROMPT_TEXT));
        originalWidthTextField = new TextField();
        originalHeightPromptLabel = new Label(props.getProperty(ORIGINAL_HEIGHT_PROMPT_TEXT));
        originalHeightTextField = new TextField();
        currentWidthPromptLabel = new Label(props.getProperty(CURRENT_WIDTH_PROMPT_TEXT));
        currentWidthSlider = new Slider();
        currentHeightPromptLabel = new Label(props.getProperty(CURRENT_HEIGHT_PROMPT_TEXT));
        currentHeightSlider = new Slider();
        xPositionPromptLabel = new Label(props.getProperty(X_POSITION_PROMPT_TEXT));
        xPositionSlider = new Slider();
        yPositionPromptLabel = new Label(props.getProperty(Y_POSITION_PROMPT_TEXT));
        yPositionSlider = new Slider();
        updateButton = new Button(props.getProperty(UPDATE_BUTTON_TEXT));
        
        // THE EDIT BUTTONS START OUT DISABLED
        addAllImagesInDirectoryButton.setDisable(true);
        addImageButton.setDisable(true);
        removeImageButton.setDisable(true);
        moveUpButton.setDisable(true);
        moveDownButton.setDisable(true);
        slideshowTitle.setDisable(true);
        undoButton.setDisable(true);
        redoButton.setDisable(true);
        
        // ARRANGE THE TABLE
        fileNameColumn = new TableColumn(props.getProperty(FILE_NAME_COLUMN_TEXT));
        currentWidthColumn = new TableColumn(props.getProperty(CURRENT_WIDTH_COLUMN_TEXT));
        currentHeightColumn = new TableColumn(props.getProperty(CURRENT_HEIGHT_COLUMN_TEXT));
        xPositionColumn = new TableColumn(props.getProperty(X_POSITION_COLUMN_TEXT));
       yPositionColumn = new TableColumn(props.getProperty(Y_POSITION_COLUMN_TEXT));
        slidesTableView.getColumns().add(fileNameColumn);
        slidesTableView.getColumns().add(currentWidthColumn);
        slidesTableView.getColumns().add(currentHeightColumn);
        slidesTableView.getColumns().add(xPositionColumn);
        slidesTableView.getColumns().add(yPositionColumn);
        fileNameColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(2));
        currentWidthColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(4));
        currentHeightColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(4));
        xPositionColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(4));
        yPositionColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(4));
        fileNameColumn.setCellValueFactory(
                new PropertyValueFactory<Slide, StringProperty>("fileName")
        );
        currentWidthColumn.setCellValueFactory(
                new PropertyValueFactory<Slide, IntegerProperty>("currentWidth")
        );
        currentHeightColumn.setCellValueFactory(
                new PropertyValueFactory<Slide, IntegerProperty>("currentHeight")
        );
        
        xPositionColumn.setCellValueFactory(
               new PropertyValueFactory<Slide, IntegerProperty>("xPosition")
       );
        
       yPositionColumn.setCellValueFactory(
                new PropertyValueFactory<Slide, IntegerProperty>("yPosition")
       );
        // HOOK UP THE TABLE TO THE DATA
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        ObservableList<Slide> model = data.getSlides();
        slidesTableView.setItems(model);
        
        // SETUP THE SLIDERS
        currentWidthSlider.setMin(0);
        currentWidthSlider.setMax(1000);
        currentWidthSlider.setBlockIncrement(200);
        currentWidthSlider.setShowTickLabels(true);
        currentWidthSlider.setMajorTickUnit(200);
        currentWidthSlider.setSnapToTicks(true);
        currentHeightSlider.setMin(0);
        currentHeightSlider.setMax(1000);
        currentHeightSlider.setBlockIncrement(200);
        currentHeightSlider.setShowTickLabels(true);
        currentHeightSlider.setMajorTickUnit(200);
        currentHeightSlider.setSnapToTicks(true);
        xPositionSlider.setMin(0);
        xPositionSlider.setMax(1000);
        xPositionSlider.setBlockIncrement(200);
        xPositionSlider.setShowTickLabels(true);
        xPositionSlider.setMajorTickUnit(200);
        xPositionSlider.setSnapToTicks(true);
        yPositionSlider.setMin(0);
        yPositionSlider.setMax(1000);
        yPositionSlider.setBlockIncrement(200);
        yPositionSlider.setShowTickLabels(true);
        yPositionSlider.setMajorTickUnit(200);
        yPositionSlider.setSnapToTicks(true);
        
        // THEM ORGANIZE THEM
        editImagesToolbar.getChildren().add(slideshowTitle);
        editImagesToolbar.getChildren().add(addAllImagesInDirectoryButton);
        editImagesToolbar.getChildren().add(addImageButton);
        editImagesToolbar.getChildren().add(removeImageButton);
        editImagesToolbar.getChildren().add(moveUpButton);
        editImagesToolbar.getChildren().add(moveDownButton);
        editImagesToolbar.getChildren().add(undoButton);
        editImagesToolbar.getChildren().add(redoButton);
        slidesTableScrollPane.setContent(slidesTableView);
        editPane.add(fileNamePromptLabel, 0, 0);
        editPane.add(fileNameTextField, 1, 0);
        editPane.add(pathPromptLabel, 0, 1);
        editPane.add(pathTextField, 1, 1);
        editPane.add(captionPromptLabel, 0, 2);
        editPane.add(captionTextField, 1, 2);
        editPane.add(originalWidthPromptLabel, 0, 3);
        editPane.add(originalWidthTextField, 1, 3);
        editPane.add(originalHeightPromptLabel, 0, 4);
        editPane.add(originalHeightTextField, 1, 4);
        editPane.add(currentWidthPromptLabel, 0, 5);
        editPane.add(currentWidthSlider, 1, 5);
        editPane.add(currentHeightPromptLabel, 0, 6);
        editPane.add(currentHeightSlider, 1, 6);
        editPane.add(xPositionPromptLabel, 0, 7);
        editPane.add(xPositionSlider, 1, 7);
        editPane.add(yPositionPromptLabel, 0, 8);
        editPane.add(yPositionSlider, 1, 8);
        editPane.add(updateButton, 0, 9);
        editPane.add(imagePane, 1, 10);
        scrollEditPane.setContent(editPane);
        
        // DISABLE THE DISPLAY TEXT FIELDS
        fileNameTextField.setDisable(true);
        pathTextField.setDisable(true);
        captionTextField.setDisable(true);
        originalWidthTextField.setDisable(true);
        originalHeightTextField.setDisable(true);
        currentWidthSlider.setDisable(true);
        currentHeightSlider.setDisable(true);
        updateButton.setDisable(true);
        xPositionSlider.setDisable(true);
        yPositionSlider.setDisable(true);
        
        // AND THEN PUT EVERYTHING INSIDE THE WORKSPACE
        app.getGUI().getTopToolbarPane().getChildren().add(editImagesToolbar);
        BorderPane workspaceBorderPane = new BorderPane();
        workspaceBorderPane.setCenter(slidesTableScrollPane);
        slidesTableScrollPane.setFitToWidth(true);
        slidesTableScrollPane.setFitToHeight(true);
        workspaceBorderPane.setRight(scrollEditPane);
        scrollEditPane.setFitToWidth(true);
        scrollEditPane.setFitToHeight(true);
        
        // AND SET THIS AS THE WORKSPACE PANE
        workspace = workspaceBorderPane;
    }
    
    private void initControllers() { 
       
        // NOW LET'S SETUP THE EVENT HANDLING
        controller = new SlideshowCreatorController(app);
        
       slideshowTitle.setOnKeyTyped(e->{ 
             undoButton.setDisable(false);
            //redoButton.setDisable(false);
            String now="";
            now=origin;
            int i=(int)e.getCharacter().charAt(0);
            if(i==8){
                if(slideshowTitle.getText().equals("")){
                    now="";
                }
                else{
                    now=now.substring(0,origin.length()-1);
                }
            }
            else{
                now=now+e.getCharacter();
            }
          controller.handleSlideshowTitle(origin, now); 
          slideshowTitle.requestFocus();
          origin=now;
          e.consume();
        
        });
        
        viewButton.setOnAction(e->{
            try {
                app.getGUI().getFileController().handleViewRequest();
                activeSlideshow();
            } catch (Exception ex) {
                Logger.getLogger(SlideshowCreatorWorkspace.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        });
        
        addAllImagesInDirectoryButton.setOnAction(e->{
            controller.handleAddAllImagesInDirectory();
             slidesTableView.getSelectionModel().clearSelection();
             clearUpdateField();
             SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
             if(data.getSlides().size()>0){
                              app.getGUI().updateViewed(false);
                          }
                          else{
                               app.getGUI().updateViewed(true);
                          }
             
        });
        addImageButton.setOnAction(e->{
             Slide result=controller.handleAddImage();
             slidesTableView.requestFocus();
            slidesTableView.getSelectionModel().select(result);
              SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
             if(data.getSlides().size()>0){
                              app.getGUI().updateViewed(false);
                          }
                          else{
                               app.getGUI().updateViewed(true);
                          }
        });
        removeImageButton.setOnAction(e->{ 
             undoButton.setDisable(false);
            //redoButton.setDisable(false);
            controller.handleRemoveImage();
            clearUpdateField();
              SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
             if(data.getSlides().size()>0){
                              app.getGUI().updateViewed(false);
                          }
                          else{
                               app.getGUI().updateViewed(true);
                          }
            
        });
        
        undoButton.setOnAction(e->{
             workspace.requestFocus();
             controller.handleUnDoTransaction();
               SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
             if(data.getSlides().size()>0){
                              app.getGUI().updateViewed(false);
                          }
                          else{
                               app.getGUI().updateViewed(true);
                          }
             redoButton.setDisable(false);
        });
        
        redoButton.setOnAction(e->{
            workspace.requestFocus();
             controller.handleDoTransaction();
               SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
             if(data.getSlides().size()>0){
                              app.getGUI().updateViewed(false);
                          }
                          else{
                               app.getGUI().updateViewed(true);
                          }
        });
        
        
       workspace.setOnKeyPressed(e->{
       if(e.isControlDown()& e.getCode()==KeyCode.Y){
              controller.handleDoTransaction();
                SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
             if(data.getSlides().size()>0){
                              app.getGUI().updateViewed(false);
                          }
                          else{
                               app.getGUI().updateViewed(true);
                          }
              
       }
        if(e.isControlDown()& e.getCode()==KeyCode.Z){
              controller.handleUnDoTransaction();
                SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
             if(data.getSlides().size()>0){
                              app.getGUI().updateViewed(false);
                          }
                          else{
                               app.getGUI().updateViewed(true);
                          }
              
          }
       });
         SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        slidesTableView.getSelectionModel().selectedItemProperty().addListener(e->{
            if(slidesTableView.getSelectionModel().getSelectedItem()==null){
                    clearUpdateField();
                    
            }
            else{
                undoButton.setDisable(false);
               // redoButton.setDisable(false);
            captionTextField.setDisable(false);
            removeImageButton.setDisable(false);
            xPositionSlider.setDisable(false);
            yPositionSlider.setDisable(false);
            if(slidesTableView.getSelectionModel().getSelectedIndex()==0){
                      moveUpButton.setDisable(true);
            }
            else{
                moveUpButton.setDisable(false);
            }
             if(slidesTableView.getSelectionModel().getSelectedIndex()==slidesTableView.getItems().size()-1){
                      moveDownButton.setDisable(true);
            }
            else{
                moveDownButton.setDisable(false);
            }
            currentWidthSlider.setDisable(false);
            currentHeightSlider.setDisable(false);
            try {
                controller.handleSelectSlide();
            } catch (MalformedURLException ex) {
                Logger.getLogger(SlideshowCreatorWorkspace.class.getName()).log(Level.SEVERE, null, ex);
            }
            }
        });
        
        moveUpButton.setOnAction(e->{
            controller.handleUp();
        });
        
        moveDownButton.setOnAction(e->{
            controller.handleDown();
        });
        captionTextField.textProperty().addListener(e->{
            controller.handleCaptionTextTyped();
        });
        currentWidthSlider.valueProperty().addListener(e->{
            controller.handleSliderMoved();
        });
        currentHeightSlider.valueProperty().addListener(e->{
            controller.handleSliderMoved();
        });
        
       xPositionSlider.valueProperty().addListener(e->{
            controller.handleSliderMoved();
        });
       
       yPositionSlider.valueProperty().addListener(e->{
            controller.handleSliderMoved();
        });
        updateButton.setOnMouseClicked(e->{
            controller.handleUpdateSlide();
            slidesTableView.getSelectionModel().clearSelection();
            clearUpdateField();
        });
    }
    
    
    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    
    private void initStyle() {
        editImagesToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        slideshowTitle.getStyleClass().add(CLASS_EDIT_BUTTON);
        addAllImagesInDirectoryButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        addImageButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        removeImageButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        moveUpButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        moveDownButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        undoButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        redoButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        
        // THE SLIDES TABLE
        slidesTableView.getStyleClass().add(CLASS_SLIDES_TABLE);
        for (TableColumn tc : slidesTableView.getColumns())
            tc.getStyleClass().add(CLASS_SLIDES_TABLE);
        
        editPane.getStyleClass().add(CLASS_BORDERED_PANE);
        fileNamePromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        fileNameTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        pathPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        pathTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        captionPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        captionTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        originalWidthPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        originalWidthTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        originalHeightPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        originalHeightTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        currentWidthPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        currentWidthSlider.getStyleClass().add(CLASS_EDIT_SLIDER);
        currentHeightPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        currentHeightSlider.getStyleClass().add(CLASS_EDIT_SLIDER);
        xPositionPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        xPositionSlider.getStyleClass().add(CLASS_EDIT_SLIDER);
        yPositionPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        yPositionSlider.getStyleClass().add(CLASS_EDIT_SLIDER);
        updateButton.getStyleClass().add(CLASS_UPDATE_BUTTON);
    }

    @Override
    public void resetWorkspace() {
        addAllImagesInDirectoryButton.setDisable(false);
        addImageButton.setDisable(false);
        slideshowTitle.setDisable(false);
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        data.resetData();
        clearUpdateField();
    }
    
    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        updateButton.setDisable(true);
        removeImageButton.setDisable(true);
         moveUpButton.setDisable(true);
         moveDownButton.setDisable(true);
         undoButton.setDisable(true);
         redoButton.setDisable(true);
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        if(data.getSlides().size()>0){
            app.getGUI().updateViewed(true);
        }
        else{
            app.getGUI().updateViewed(false);
        }
        slideshowTitle.setText(data.getSlideShowTitle());
         origin=slideshowTitle.getText();
    }
    
    public void activeSlideshow(){ 
        title.setText(slideshowTitle.getText());
        title.setStyle("-fx-font-size:22pt; -fx-padding: 10; -fx-spacing: 10; ");
        String buttonCSS="-fx-font-size: 14pt;  -fx-font-weight: bold; -fx-border-radius: 10 10 10 10;-fx-background-radius: 15 15 15 15;"
                + "-fx-background-color: #FFFF99;"+"-fx-border-color:#e0ff99;";
        playButton.setStyle(buttonCSS);
        stopButton.setStyle(buttonCSS);
        nextButton.setStyle(buttonCSS);;
        previousButton.setStyle(buttonCSS);
        app.getGUI().getViewPane().getChildren().clear();
        app.getGUI().getTitle().getChildren().clear();
        app.getGUI().getTitle().getChildren().add(title);
        app.getGUI().getControl().getChildren().clear();
        app.getGUI().getControl().getChildren().add(previousButton);
        app.getGUI().getControl().getChildren().add(playButton);
        app.getGUI().getControl().getChildren().add(stopButton);
        app.getGUI().getControl().getChildren().add(nextButton);
        app.getGUI().getViewPane().setTop(app.getGUI().getTitle());
        viewBox.getChildren().clear();
        app.getGUI().getViewPane().setCenter(viewBox);
        app.getGUI().getViewPane().setBottom(app.getGUI().getControl()); 
        try{
            controller.handleViewSlideshow();
        } catch (MalformedURLException ex) {
            Logger.getLogger(SlideshowCreatorWorkspace.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
     public void clearUpdateField(){
        currentWidthSlider.setDisable(true);
       currentHeightSlider.setDisable(true);
       xPositionSlider.setDisable(true);
       yPositionSlider.setDisable(true);
         imagePane.setRight(null);
         slideshowTitle.clear();
       fileNameTextField.clear();
       pathTextField.clear();
       captionTextField.clear();
       captionTextField.setDisable(true);
       originalWidthTextField.clear();
       originalHeightTextField.clear();
       currentWidthSlider.setValue(0);
       currentHeightSlider.setValue(0);
       xPositionSlider.setValue(0);
       yPositionSlider.setValue(0);
       updateButton.setDisable(true);
       removeImageButton.setDisable(true);
       moveUpButton.setDisable(true);
       moveDownButton.setDisable(true);
      
               }
     
     public TextField getSlideshowTitle(){
         return slideshowTitle;
     }
     
     public TableView getSlideTable(){
         return  slidesTableView;
     }
     public void setVBox(Node n){
         viewBox.getChildren().add(n);
     }
}