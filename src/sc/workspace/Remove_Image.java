/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sc.workspace;

import djf.ui.AppMessageDialogSingleton;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import sc.data.Slide;
import sc.data.SlideshowCreatorData;

/**
 *
 * @author xiangying liu
 */
public class Remove_Image implements jTPS_Transaction{
    private SlideshowCreatorData data;
    private SlideshowCreatorWorkspace workspace;
    private Slide sl;
    private int index;
    public Remove_Image(SlideshowCreatorData initData, SlideshowCreatorWorkspace initWorkspace, Slide initSl, int initIndex){
        data=initData;
        workspace=initWorkspace;
        sl=initSl;
        index=initIndex;
    }

    @Override
    public void doTransaction() {
        data.remove(sl.getFileName(), sl.getPath());
        workspace.getSlideTable().getSelectionModel().clearSelection();
         workspace.clearUpdateField();
         workspace.getSlideTable().refresh();
    }

    @Override
    public void undoTransaction() {
         ObservableList<Slide> tableData = data.getSlides();
         Slide newSl=new Slide(sl.getFileName(), sl.getPath(), sl.getCaption(), sl.getOriginalWidth(), sl.getOriginalHeight());
        tableData.add(index, sl);
       // workspace.getSlideTable().setItems(tableData);
        workspace.getSlideTable().requestFocus();
           workspace.getSlideTable().getSelectionModel().select(index);
           workspace.getSlideTable().refresh();
    }
}
