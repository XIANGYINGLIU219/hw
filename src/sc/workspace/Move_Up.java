/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sc.workspace;

import djf.ui.AppMessageDialogSingleton;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import sc.data.Slide;
import sc.data.SlideshowCreatorData;

/**
 *
 * @author xiangying liu
 */
public class Move_Up implements jTPS_Transaction{
    private SlideshowCreatorData data;
    private SlideshowCreatorWorkspace workspace;
    private int index;
    private Slide sl;
    public Move_Up(SlideshowCreatorData initData, SlideshowCreatorWorkspace initWorkspace, int initIndex, Slide initSlide){
        data=initData;
        workspace=initWorkspace;
        index=initIndex;
        sl=initSlide;
    }
    @Override
    public void doTransaction() {
         int i=workspace.getSlideTable().getSelectionModel().getSelectedIndex();
         i=i+1;
      if(index!=i){
          ObservableList<Slide> tableData = data.getSlides();
         tableData.remove(sl);
         tableData.add(index-1, sl);
      }
        workspace.getSlideTable().requestFocus();
       workspace.getSlideTable().getSelectionModel().select(sl);

    }

    @Override
    public void undoTransaction() {
         ObservableList<Slide> tableData = data.getSlides();
         Slide sl=(Slide)workspace.getSlideTable().getSelectionModel().getSelectedItem();
         tableData.remove(sl);
         tableData.add(index, sl);
         workspace.getSlideTable().requestFocus();
         workspace.getSlideTable().getSelectionModel().select(sl);
         
    }
    
}
