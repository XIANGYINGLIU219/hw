/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sc.workspace;


import djf.ui.AppMessageDialogSingleton;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import sc.data.Slide;
import sc.data.SlideshowCreatorData;

/**
 *
 * @author xiangying liu
 */
public class Add_All_Image implements jTPS_Transaction{
    private SlideshowCreatorWorkspace workspace;
    private SlideshowCreatorData data;
    private ObservableList<Slide> tableData;
    
    public Add_All_Image(SlideshowCreatorData initData,SlideshowCreatorWorkspace initWorkspace,ObservableList<Slide> initTableData){
       data=initData;
        tableData=initTableData;
        workspace=initWorkspace;
    }

    @Override
    public void doTransaction() {
           for(Slide sl: tableData){
               data.addNonDuplicateSlide(sl.getFileName(), sl.getPath(),sl.getCaption(),sl.getOriginalWidth(),sl.getOriginalHeight());
           }
            workspace.getSlideTable().setItems(data.getSlides());
            workspace.clearUpdateField();
            workspace.getSlideTable().getSelectionModel().clearSelection();
    }

    @Override
    public void undoTransaction() {
       for(int i=0; i<tableData.size();i++){
           data.remove(tableData.get(i).getFileName(), tableData.get(i).getPath());
       }
         workspace.getSlideTable().setItems(data.getSlides());
          workspace.getSlideTable().getSelectionModel().clearSelection();
    }
    
}
