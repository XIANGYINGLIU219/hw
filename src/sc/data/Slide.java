package sc.data;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This data class represents a single slide in a slide show. Note that
 * it has information regarding the path to the file, the caption for
 * the slide, and sizing information.
 */
public class Slide {
    private StringProperty fileName;
    private StringProperty path;
    private StringProperty caption;
    private IntegerProperty originalWidth;
    private IntegerProperty originalHeight;
    private IntegerProperty currentWidth;
    private IntegerProperty currentHeight;
     private IntegerProperty xPosition;
    private IntegerProperty  yPosition;
    
    public Slide(   String initFileName,
                    String initPath, 
                    String initCaption,
                    int initOriginalWidth,
                    int initOriginalHeight
                       ) {
        fileName = new SimpleStringProperty(initFileName);
        path = new SimpleStringProperty(initPath);
        caption = new SimpleStringProperty(initCaption);
        originalWidth = new SimpleIntegerProperty(initOriginalWidth);
        originalHeight= new SimpleIntegerProperty(initOriginalHeight);
        currentWidth = new SimpleIntegerProperty(initOriginalWidth);
        currentHeight = new SimpleIntegerProperty(initOriginalHeight);
        xPosition= new SimpleIntegerProperty((int)0);
        yPosition= new SimpleIntegerProperty((int)0);
    }
    
    // ACCESSORS AND MUTATORS
    
    public StringProperty getFileNameProperty() {
        return fileName;
    }
    public String getFileName() {
        return fileName.getValue();
    }
    public void setFileName(String initFileName) {
        fileName.setValue(initFileName);
    }
    
    public StringProperty getPathProperty() {
        return path;
    }
    public String getPath() {
        return path.getValue();
    }
    public void setPath(String initPath) {
        path.setValue(initPath);
    }
    
    public StringProperty getCaptionProperty() {
        return caption;
    }
    public String getCaption() {
        return caption.getValue();
    }
    public void setCaption(String initCaption) {
        caption.setValue(initCaption);
    }
    
    public IntegerProperty getOriginalWidthProperty() {
        return originalWidth;
    }
    public Integer getOriginalWidth() {
        return originalWidth.getValue();
    }
    public void setOriginalWidth(Integer initOriginalWidth) {
        originalWidth.setValue(initOriginalWidth);
    }
    
    public IntegerProperty getOriginalHeightProperty() {
        return originalHeight;
    }
    public Integer getOriginalHeight() {
        return originalHeight.getValue();
    }
    public void setOriginalHeight(Integer initOriginalHeight) {
        originalHeight.setValue(initOriginalHeight);
    }
    
    public IntegerProperty getCurrentWidthProperty() {
        return currentWidth;
    }
    public Integer getCurrentWidth() {
        return currentWidth.getValue();
    }
    public void setCurrentWidth(Integer initCurrentWidth) {
        currentWidth.setValue(initCurrentWidth);
    }
    
    public IntegerProperty getCurrentHeightProperty() {
        return currentHeight;
    }
    public Integer getCurrentHeight() {
        return currentHeight.getValue();
    }
    public void setCurrentHeight(Integer initCurrentHeight) {
        currentHeight.setValue(initCurrentHeight);
    }
    
      public IntegerProperty getXPositionProperty() {
        return xPosition;
    }
    public Integer getXPosition() {
        return xPosition.getValue();
    }
    public void setXPosition(Integer initXPosition) {
        xPosition.setValue(initXPosition);
    }
    
       public IntegerProperty getYPositionProperty() {
        return yPosition;
    }
    public Integer getYPosition() {
        return yPosition.getValue();
    }
    public void setYPosition(Integer initYPosition) {
        yPosition.setValue(initYPosition);
    }
}
