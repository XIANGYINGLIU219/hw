package sc.data;

import javafx.collections.ObservableList;
import djf.components.AppDataComponent;
import djf.ui.AppMessageDialogSingleton;
import javafx.collections.FXCollections;
import javafx.scene.control.TableView;
import sc.SlideshowCreatorApp;
import sc.workspace.SlideshowCreatorWorkspace;

/**
 * This is the data component for SlideshowCreatorApp. It has all the data needed
 * to be set by the user via the User Interface and file I/O can set and get
 * all the data from this object
 * 
 * @author Richard McKenna
 */
public class SlideshowCreatorData implements AppDataComponent {

    // WE'LL NEED ACCESS TO THE APP TO NOTIFY THE GUI WHEN DATA CHANGES
    SlideshowCreatorApp app;

    // NOTE THAT THIS DATA STRUCTURE WILL DIRECTLY STORE THE
    // DATA IN THE ROWS OF THE TABLE VIEW
    ObservableList<Slide> slides;
    String slideShowTitle;
    /**
     * This constructor will setup the required data structures for use.
     * 
     * @param initApp The application this data manager belongs to. 
     */
    public SlideshowCreatorData(SlideshowCreatorApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        
        // MAKE THE SLIDES MODEL
        slides = FXCollections.observableArrayList();
        slideShowTitle="";
    }
    
    // ACCESSOR METHOD
    public ObservableList<Slide> getSlides() {
        return slides;
    }
    
    public String getSlideShowTitle(){
        return slideShowTitle;
    }
    
    public void setSlideShowTitle(String initTitle){
        slideShowTitle=initTitle;
    }
    
    /**
     * Called each time new work is created or loaded, it resets all data
     * and data structures such that they can be used for new values.
     */
    @Override
    public void resetData() {
        slides.clear();
        slideShowTitle="";
    }

    // FOR ADDING A SLIDE WHEN THERE ISN'T A CUSTOM SIZE
    public void addSlide(String fileName, String path, String caption, int originalWidth, int originalHeight) {
        Slide slideToAdd = new Slide(fileName, path, caption, originalWidth, originalHeight);
        slides.add(slideToAdd);
    }

    // FOR ADDING A SLIDE WITH A CUSTOM SIZE
    public void addSlide(String fileName, String path, String caption, int originalWidth, int originalHeight, int currentWidth, int currentHeight, int xPosition, int yPosition) {
        Slide slideToAdd = new Slide(fileName, path, caption, originalWidth, originalHeight);
        slideToAdd.setCurrentWidth(currentWidth);
        slideToAdd.setCurrentHeight(currentHeight);
        slideToAdd.setXPosition(xPosition);
        slideToAdd.setYPosition(yPosition);
        slides.add(slideToAdd);
    }
    
    public void addNonDuplicateSlide(String fileName, String path, String caption, int originalWidth, int originalHeight) {
        if (!hasSlide(fileName, path)){
            addSlide(fileName, path, caption, originalWidth, originalHeight);    }    
    }
    
    public void addNonDuplicateSlide(String fileName, String path, String caption, int originalWidth, int originalHeight, int currentWidth, int currentHeight,int xPosition, int yPosition) {
        if (!hasSlide(fileName, path))
            addSlide(fileName, path, caption, originalWidth, originalHeight, currentWidth, currentHeight,xPosition ,yPosition);        
    }
    
    public boolean hasSlide(String testFileName, String testPath) {
        for (Slide s : slides) {
            if (s.getFileName().equals(testFileName)
                    && s.getPath().equals(testPath))
                return true;
        }
        return false;
    }
    
    public void removeSlide(Slide slideToRemove) {  
        slides.remove(slideToRemove);
    }
    
   public void remove(String fileName, String path){
       for(int i = 0; i<slides.size() ; i++){ 
                       if (slides.get(i).getFileName().equals(fileName)&&slides.get(i).getPath().equals(path)) {
                         slides.remove(i);
                         
                       } 
            }
   
   }
    
}